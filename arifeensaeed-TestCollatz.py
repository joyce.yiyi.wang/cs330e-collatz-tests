9#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 2)

    def test_read_3(self):
        s = "40 40\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  40)
        self.assertEqual(j, 40)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10, {})
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200, {})
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210, {})
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000, {})
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(9, 9, {})
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(999999, 999990, {})
        self.assertEqual(v, 259)

    def test_eval_7(self):
        v = collatz_eval(10, 1, {})
        self.assertEqual(v, 20)

    def test_eval_8(self):
        v = collatz_eval(1, 1, {})
        self.assertEqual(v, 1)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 40, 119)
        self.assertEqual(w.getvalue(), "100 40 119\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 600, 600, 18)
        self.assertEqual(w.getvalue(), "600 600 18\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 999999, 999990, 259)
        self.assertEqual(w.getvalue(), "999999 999990 259\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("2 30\n500 500\n999999 999990\n8000 8001\n547 450\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 30 112\n500 500 111\n999999 999990 259\n8000 8001 115\n547 450 142\n")

    def test_solve_3(self):
        r = StringIO("69 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "69 100 119\n")

    def test_solve_4(self):
        r = StringIO("100 200\n1000 2000\n10000 20000\n100000 100020\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100 200 125\n1000 2000 182\n10000 20000 279\n100000 100020 310\n")

    def test_solve_5(self):
        r = StringIO("300 329\n100000 109998\n20000 29999\n30000 39999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "300 329 144\n100000 109998 354\n20000 29999 308\n30000 39999 324\n")

    def test_solve_6(self):
        r = StringIO("20 30\n200 300\n400 410\n900 1000\n90000 99999\n35 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "20 30 112\n200 300 128\n400 410 41\n900 1000 174\n90000 99999 333\n35 1 112\n")

    def test_solve_7(self):
        r = StringIO("1 1\n500 500\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n500 500 111\n")

# phase III tests using lazy_cache
    def test_solve_8(self):
        r = StringIO("1 10000\n3000 6000\n4000 8000\n1500 6500\n1000 9000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10000 262\n3000 6000 238\n4000 8000 262\n1500 6500 262\n1000 9000 262\n")

    def test_solve_9(self):
        r = StringIO("1 100000\n10000 50000\n75000 90000\n100 90000\n4750 47500\n50000 1\n67500 500\n48000 320\n65000 45\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 100000 351\n10000 50000 324\n75000 90000 351\n100 90000 351\n4750 47500 324\n50000 1 324\n67500 500 340\n48000 320 324\n65000 45 340\n")

    def test_solve_10(self):
        r = StringIO("1 999999\n100000 400000\n500000 900000\n250000 750000\n50000 500000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n100000 400000 443\n500000 900000 525\n250000 750000 509\n50000 500000 449\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
