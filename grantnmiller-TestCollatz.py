#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(900,'potato')
        self.assertEqual(v, 'Please enter 2 numbers')

    def test_eval_6(self):
        v = collatz_eval('not a number','also not a number')
        self.assertEqual(v, 'Please enter 2 numbers')

    def test_eval_7(self):
        v = collatz_eval('not a number',672)
        self.assertEqual(v, 'Please enter 2 numbers')

    def test_eval_8(self):
        v = collatz_eval(672,'po')
        self.assertEqual(v, 'Please enter 2 numbers')

    def test_eval_9(self):
        v = collatz_eval(672,1)
        self.assertEqual(v, 145)

    def test_eval_10(self):
        v = collatz_eval(210,201)
        self.assertEqual(v, 89)

    def test_eval_11(self):
        v = collatz_eval(1000,900)
        self.assertEqual(v, 174)

    def test_eval_12(self):
        v = collatz_eval(-10,900)
        self.assertEqual(v, 'Please input positive intergers within range')

    def test_eval_13(self):
        v = collatz_eval(10,999999900)
        self.assertEqual(v, 'Please input positive intergers within range')

    def test_eval_14(self):
        v = collatz_eval(-10,999999999)
        self.assertEqual(v, 'Please input positive intergers within range')

    def test_eval_15(self):
        v = collatz_eval(-10,10)
        self.assertEqual(v, 'Please input positive intergers within range')
        
    def test_eval_16(self):
        v = collatz_eval(990, 1000)
        self.assertEqual(v, 112)

    def test_eval_17(self):
        v = collatz_eval(1000, 990)
        self.assertEqual(v, 112)




    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
